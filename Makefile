CFLAGS = -Wall `pkg-config --cflags MLV`
LDFLAGS = `pkg-config --libs MLV`
bin = bin/Main.o bin/Structures.o bin/Actions.o bin/Graphic.o

fight: Main.o Structures.o Actions.o Graphic.o
		gcc -o fight $(bin) $(CFLAGS) $(LDFLAGS)

Main.o: src/Main.c include/Actions.h
	gcc -o bin/$@ -c $< $(CFLAGS) $(LDFLAGS)

Structures.o: src/Structures.c include/Structures.h
	gcc -o bin/$@ -c $< $(CFLAGS) $(LDFLAGS)

Graphic.o: src/Graphic.c include/Graphic.h include/Structures.h
	gcc -o bin/$@ -c $< $(CFLAGS) $(LDFLAGS)

Actions.o: src/Actions.c include/Actions.h include/Graphic.h
	gcc -o bin/$@ -c $< $(CFLAGS) $(LDFLAGS)

clean:
	rm -Rf bin/*.o

mrproper: clean
	rm -f fight
