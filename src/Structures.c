/* DE CASTRO, COMBE
 * TD2
 * Structures.c */

#include "../include/Structures.h"
#include <stdio.h>
#include <string.h>

int creerUnite(char type, UListe *unite){
    UListe tmp;

    if (NULL == (tmp = malloc(sizeof(Unite))))
        return 0;

    tmp->genre = type;
    tmp->suiv = *unite;
    *unite = tmp;
    return 1;
}
/*
int creerCase(CListe *Case){
	CListe tmp;

	if (NULL == (tmp = malloc(sizeof(Case))))
		return 0;
	tmp->taille = 0;
	tmp->ulst = NULL;
	tmp->suiv = *Case;
	*Case = tmp;
	return 1;
}*/

void initializerMonde(Monde *monde){
    int i, j;
    monde->tour = 0;

    for (i = 0; i < LONG; i++){
        for (j = 0; j < LARG; j++){
            monde->plateau[i][j] = NULL;
        }
    }
    monde->rouge = NULL;
    monde->bleu = NULL;
	monde->potion = NULL;
}

int randomInt(int nb){
     return rand()%(nb);
}

int ajouterDansCase(Unite *unite, Case *Case){
	
	if (Case->taille < 4){
		if ((unite = malloc(sizeof(Unite))) == NULL)
			return 0;
		
		unite->suiv = Case->ulst->suiv;
		Case->ulst->suiv = unite;
		Case->taille++;
		while(Case->ulst != NULL){
			printf("LISTE CASE : %d %d\n", Case->ulst->posX, Case->ulst->posY);
			Case->ulst = Case->ulst->suiv;
		}
		return 1;
	}
	return -1;
}

int dejaOccupee(UListe lst, int posX, int posY){
    UListe tmp = lst;
    while (tmp != NULL){
        if ((tmp->posX == posX) && (tmp->posY == posY))
            return 1;
        tmp = tmp->suiv;
    }
    return 0;
}
	
int placerAuMonde(Unite *unite, Monde *monde, int posX, int posY, char couleur){
    if (posX < 0 || posY < 0 || posX >= LARG || posY >= LONG)
        return 0;
    if (dejaOccupee(monde->rouge, posX, posY) == 1 || dejaOccupee(monde->bleu, posX, posY) == 1 ||  dejaOccupee(monde->potion, posX, posY) == 1)
         return 0;
	Case caseActive;
	caseActive.posX = posX;
	caseActive.posY = posY;
    unite->posX = posX;
    unite->posY = posY;
    unite->couleur = couleur;
	switch(couleur){
		case ROUGE :
        	unite->suiv = monde->rouge;
        	monde->rouge = unite;
			break;
		case BLEU :
        	unite->suiv = monde->bleu;
        	monde->bleu = unite;
			break;
		case POTION :
			unite->suiv = monde->potion;
			monde->potion = unite;
			break;
	}
    monde->plateau[caseActive.posY][caseActive.posX] = caseActive.ulst;
    return 1;
}

 int uniteExist(UListe lst, Unite unite){
     UListe tmp = lst;

     while (tmp != NULL){
         if ((tmp->couleur == unite.couleur) && (tmp->genre == unite.genre) && (tmp->posX == unite.posX) && (tmp->posY == unite.posY))
             return 1;
         tmp = tmp->suiv;
     }
     return 0;
 }

 int deplacerUnite(Unite *unite, Monde *monde, int destX, int destY){
    int x, y;
    x = unite->posX;
    y = unite->posY;
    UListe tmp;
	Case caseActive;
	caseActive.posX = destX;
	caseActive.posY = destY;
    if (uniteExist(monde->rouge, *unite) == 0 && uniteExist(monde->bleu, *unite) == 0)
        return 0;
    if (dejaOccupee(monde->rouge, destX, destY) == 1 || dejaOccupee(monde->bleu, destX, destY) == 1){
		ajouterDansCase(unite, &caseActive);
        //return 0;
	}

    if (unite->couleur == ROUGE)
        tmp = monde->rouge;
    else
        tmp = monde->bleu;

     while (tmp != NULL){
         if ((tmp->couleur == unite->couleur) && (tmp->genre == unite->genre) && (tmp->posX == unite->posX) && (tmp->posY == unite->posY)){
             tmp->posX = destX;
             tmp->posY = destY;
             unite->posX = destX;
             unite->posY = destY;
             monde->plateau[y][x] = NULL;
             monde->plateau[destY][destX] = unite;
             return 1;
         }
         tmp = tmp->suiv;
     }
     return 0;
 }

 int suppUniteListe(UListe *lst, Unite *unite, Monde *monde){
     UListe debut = *lst;
     if (*(lst) == NULL)
         return 0;
     if (((*lst)->couleur == unite->couleur) && ((*lst)->genre == unite->genre) && ((*lst)->posX == unite->posX) && ((*lst)->posY == unite->posY)){
         (*lst) = (*lst)->suiv;
         monde->plateau[unite->posY][unite->posX] = NULL;
         free(unite);
         return 1;
     }
     while ((*lst)->suiv != NULL){
         if (((*lst)->suiv->couleur == unite->couleur) && ((*lst)->suiv->genre == unite->genre) && ((*lst)->suiv->posX == unite->posX) && ((*lst)->suiv->posY == unite->posY)){
             (*lst)->suiv = (*lst)->suiv->suiv;
             monde->plateau[unite->posY][unite->posX] = NULL;
             *lst = debut;
             free(unite);
             return 1;
         }
         (*lst) = (*lst)->suiv;
     }
     return 0;
 }

int enleverUnite(Unite *unite, Monde *monde){
	if (uniteExist(monde->rouge, *unite) == 0 && uniteExist(monde->bleu, *unite) == 0 && uniteExist(monde->potion, *unite) == 0)
		return 0;
	if (unite->couleur == ROUGE)
		return suppUniteListe(&(monde->rouge), unite, monde);
	if (unite->couleur == BLEU)
		return suppUniteListe(&(monde->bleu), unite, monde);
	if (unite->couleur == POTION)
		return suppUniteListe(&(monde->potion), unite, monde);
	return 0;
}

int insererUnite(UListe lst, Monde *monde, char couleur){
	Unite *unite;
	Case *caseActive;
	
	if ((unite = malloc(sizeof(Unite))) == NULL)
		return 0;
	int posX = randomInt(18);
	int posY = randomInt(12);
	caseActive->posX = posX;
	caseActive->posY = posY;

	while (dejaOccupee(lst, posX, posY) == 1){
		posX = randomInt(18);
		posY = randomInt(12);
	}
	unite->posX = posX;
	unite->posY = posY;
	unite->genre = SERF;
	unite->couleur = couleur;
	ajouterDansCase(unite, caseActive);
	placerAuMonde(unite, monde, posX, posY, couleur);
	return 1;
}

int attaquer(Unite *unite, Monde *monde, int posX, int posY){
    if (unite->genre == monde->plateau[posY][posX]->ulst->genre){
        enleverUnite(monde->plateau[posY][posX]->ulst, monde);
        return 1;
    }
    if (unite->genre == GUERRIER && monde->plateau[posY][posX]->ulst->genre == SERF){
        enleverUnite(monde->plateau[posY][posX]->ulst, monde);
        return 1;
    }
    if (unite->genre == SERF && monde->plateau[posY][posX]->ulst->genre == GUERRIER){
        enleverUnite(unite, monde);
        return 0;
    }
	if(unite->genre == SERF && monde->plateau[posY][posX]->ulst->genre == MORT){
		enleverUnite(monde->plateau[posY][posX]->ulst, monde);
		enleverUnite(unite, monde);
		return 0;
	}
	if(unite->genre == GUERRIER && monde->plateau[posY][posX]->ulst->genre == MORT){
		enleverUnite(monde->plateau[posY][posX]->ulst, monde);
		unite->genre = SERF;
		return 0;
	}

	if(unite->genre == SERF && monde->plateau[posY][posX]->ulst->genre == VIE){
		enleverUnite(monde->plateau[posY][posX]->ulst, monde);
		unite->genre = GUERRIER;
		return 2;
	}
	if(unite->genre == GUERRIER && monde->plateau[posY][posX]->ulst->genre == VIE){
		enleverUnite(monde->plateau[posY][posX]->ulst, monde);
		if(unite->couleur == ROUGE)
			insererUnite(monde->rouge, monde, ROUGE);
		if(unite->couleur == BLEU)
			insererUnite(monde->bleu, monde, BLEU);
		else
			return -1;
		return 3;
	}

    return -1;
}

int estVoisine(Unite unite, int destX, int destY){
    if (destX >= unite.posX-1 && destX <= unite.posX+1 && destY >= unite.posY-1 && destY <= unite.posY+1)
        return 1;
    return 0;
}

void viderMonde(Monde *monde){
    UListe tmpsuiv;
    int i, j;

    while(monde->rouge != NULL){
        tmpsuiv = monde->rouge->suiv;
        free(monde->rouge);
        monde->rouge = tmpsuiv;
    }
    while(monde->bleu != NULL){
        tmpsuiv = monde->bleu->suiv;
        free(monde->bleu);
        monde->bleu = tmpsuiv;
    }
	 while(monde->potion != NULL){
        tmpsuiv = monde->potion->suiv;
        free(monde->potion);
        monde->potion = tmpsuiv;
    }
    for (i = 0; i < LONG; i++){
        for (j = 0; j < LARG; j++){
            if (monde->plateau[i][j] != NULL)
                monde->plateau[i][j] = NULL;
        }
    }
    monde->rouge = NULL;
    monde->bleu = NULL;
    monde->potion = NULL;
}
