/* DE CASTRO, COMBE
 * TD2
 * Actions.c */

#include "../include/Graphic.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <MLV/MLV_all.h>

int deplacerOuAttaquer(Unite *unite, Monde *monde, int destX, int destY){
	if (destX < 0 || destY < 0 || destX >= LARG || destY >= LONG)
		return -1;
	if (estVoisine(*unite, destX, destY) == 0)
		return -2;
	if (monde->plateau[destY][destX] == NULL){
		deplacerUnite(unite, monde, destX, destY);
		return 1;
	}
	if (unite->couleur == monde->plateau[destY][destX]->couleur && (dejaOccupee(monde->rouge, destX, destY) == 1 || dejaOccupee(monde->bleu, destX, destY) == 1))
		return -3;
	else{
		int res = attaquer(unite, monde, destX, destY);
		if (res == 1)
	 		return 2;
		else
	 		return 3;
	}
}

void gereCoordonnees(UListe tmp, Monde *monde, int x, int y, MLV_Font * font2){
	int i, j, res;

	for (i = 0; i < LONG; i++){
		for(j = 0; j < LARG; j++){
			if(j*CASE <= x && x <= (j*CASE)+CASE && i*CASE <= y && y <= (i*CASE)+CASE){
				if((res = deplacerOuAttaquer(tmp, monde, j, i)) < 0){
					switch (res){
						case -1 : afficheErreur("CASE NON VALIDE", font2); break;
						case -2 : afficheErreur("CASE NON VOISINE", font2); break;
						case -3 : afficheErreur("CASE ALLIEE", font2); break;
						default : afficheErreur("ERREUR", font2); break;
					}
				}
				else
					affichePlateau(*monde);
			}
		}
	}
}

int gereClic(UListe tmp, Monde *monde, int x, int y,  MLV_Font * font2){
	if (0 <= x && x <= CASE*LARG && 0 <= y && y <= CASE*LONG){
		gereCoordonnees(tmp, monde, x ,y, font2);
		return 1;
	}
	if ((CASE*LARG)+2 <= x && x <= (CASE*LARG)+250 && 200 <= y && y < 250)
		return 2;
	if ((CASE*LARG)+2 <= x && x <= (CASE*LARG)+250 && 250 <= y && y < 300)
		return 3;
	else
		return 0;
}

void gererDemiTour(char joueur, Monde *monde, MLV_Font * font, MLV_Font * font2){
    UListe tmp1;
	int x,y;
	if(joueur == ROUGE){
		tmp1 = monde->rouge;
	}
	else{
		tmp1 = monde->bleu;
	}

	while (tmp1 != NULL){
		affichePlateau(*monde);
		/* ERREUR ICIIIIII comme s'il connaissait pas tmp1, il accède a quelque chose de null ou qui n'existe pas car même le printf bug */
		afficheDonnees(joueur, monde->nom1, monde->nom2, tmp1->posX, tmp1->posY, font);
		MLV_wait_mouse(&x, &y);

		if(gereClic(tmp1, monde, x, y, font2) == 3)
			return;
		else if (monde->rouge == NULL || monde->bleu == NULL){
			return;
		}
		else
			tmp1 = tmp1->suiv;
	}
}

/*Laisse les deux joueurs jouer (toujours dans le même ordre) et qui met à jour le compteur de tours*/
void gererTour(Monde *monde, MLV_Font * font, MLV_Font * font2, int nb){
	if (nb == 0){
		gererDemiTour(ROUGE, monde, font, font2);
		if(monde->bleu == NULL)
			return;
		gererDemiTour(BLEU, monde, font, font2);
		if(monde->rouge == NULL)
			return;
	}
	else{
		gererDemiTour(BLEU, monde, font, font2);
		if(monde->rouge == NULL)
			return;
		gererDemiTour(ROUGE, monde, font, font2);
		if(monde->bleu == NULL)
			return;
	}
	monde->tour++;
}



//int createListe(UListe lst, PListe lst2, Monde *monde, char couleur, int nb){
int createListe(UListe lst, Monde *monde, char couleur, int nb){
	int i;
    UListe u;

    for(i = 0; i < nb; i++){
		if(couleur == ROUGE || couleur == BLEU){
			if(i < nb/2){
				if(creerUnite(GUERRIER, &u) == 0)
					return 0;
			}
			if(i >= nb/2){
				if(creerUnite(SERF, &u) == 0)
					return 0;
			}
		}
		if(couleur == POTION){
			if(i < nb/2){
				if(creerUnite(MORT, &u) == 0)
					return 0;
			}
			if(i >= nb/2){
				if(creerUnite(VIE, &u) == 0)
					return 0;
			}
		}
        while (placerAuMonde(u, monde, randomInt(18), randomInt(12), couleur) == 0){}
    }
    return 1;
}


void gererPartie(void){
    Monde monde;
	int nb = randomInt(2);
	char gagnant[255];

	fenetre();
	MLV_Font *font3 = MLV_load_font("font/sing_14l.ttf", 120);
	MLV_Font *font = MLV_load_font("font/sing_14l.ttf", 60);
	MLV_Font *font2 = MLV_load_font("font/HelveticaNeue.ttf", 14);
	initializerMonde(&monde);
	accueil(&monde, font3, font2);
    createListe(monde.rouge, &monde, ROUGE, 3);
    createListe(monde.bleu, &monde, BLEU, 3);
    createListe(monde.potion, &monde, POTION, 6);
	affichePlateau(monde);
	dessineOptions(font2);
	MLV_actualise_window();

   	do{
        gererTour(&monde, font, font2, nb);
    }while(monde.rouge != NULL && monde.bleu != NULL);

    if (monde.bleu == NULL){
		strcat(gagnant, monde.nom1);
		strcat(gagnant, " GAGNE !");
		afficheGagnant(gagnant, font3);
	}
    if (monde.rouge == NULL){
		strcat(gagnant, monde.nom2);
		strcat(gagnant, " GAGNE !"),
        afficheGagnant(gagnant, font3);
	}
	MLV_wait_seconds(3);
	MLV_free_font(font);
	MLV_free_font(font2);
	MLV_free_font(font3);
	MLV_free_window();
    viderMonde(&monde);
}
