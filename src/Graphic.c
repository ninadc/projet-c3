/* DE CASTRO, COMBE
 * TD2
 * Graphic.c */

#include "../include/Graphic.h"
#include <stdio.h>


void fenetre(){
	MLV_create_window("FIGHT", "shapes", CASE*LARG+250, CASE*LONG);
	MLV_draw_filled_rectangle(0, 0, CASE*LARG+250,CASE*LONG, MLV_COLOR_WHITE);
}

void affichePlateau(Monde monde){
	int i, j;
	MLV_draw_filled_rectangle(0, 0, CASE*LARG,CASE*LONG, MLV_COLOR_WHITE);
	dessineCadrillage();

	for (i = 0; i < LONG; i++){
		for (j = 0; j < LARG; j++) {
			if (monde.plateau[i][j] != NULL){
				switch(monde.plateau[i][j]->ulst->genre){
					case GUERRIER :
						dessineGuerrier(monde.plateau[i][j]->ulst->couleur, monde.plateau[i][j]->ulst->posX, monde.plateau[i][j]->ulst->posY);
						break;
					case SERF :
						dessineSerf(monde.plateau[i][j]->ulst->couleur, monde.plateau[i][j]->ulst->posX, monde.plateau[i][j]->ulst->posY);
						break;
					case MORT :
						dessineMort(monde.plateau[i][j]->ulst->posX, monde.plateau[i][j]->ulst->posY);
						break;
					case VIE :
						dessineVie(monde.plateau[i][j]->ulst->posX, monde.plateau[i][j]->ulst->posY);
						break;
				}
			}
		}
	}
	MLV_actualise_window();
}

void afficheDonnees(char couleur, char *nom1, char *nom2, int posX, int posY, MLV_Font * font){
	int lg = 2;
	MLV_draw_filled_rectangle(CASE*LARG+2,0,250, 130,MLV_COLOR_WHITE);
	if (couleur == ROUGE){
		MLV_draw_text_with_font((CASE*LARG)+8, 45, nom1, font, MLV_COLOR_RED);
		MLV_draw_rectangle(posX*CASE, posY*CASE, CASE,lg, MLV_COLOR_RED);
		MLV_draw_rectangle(posX*CASE, posY*CASE, lg,CASE, MLV_COLOR_RED);
		MLV_draw_rectangle((posX*CASE)+CASE-(lg/2), posY*CASE, lg,CASE, MLV_COLOR_RED);
		MLV_draw_rectangle(posX*CASE, (posY*CASE)+CASE-(lg/2), CASE, lg, MLV_COLOR_RED);
	}
	else{
		MLV_draw_text_with_font((CASE*LARG)+8, 45, nom2, font, MLV_COLOR_BLUE);
		MLV_draw_rectangle(posX*CASE, posY*CASE, CASE,lg, MLV_COLOR_BLUE);
		MLV_draw_rectangle(posX*CASE, posY*CASE, lg,CASE, MLV_COLOR_BLUE);
		MLV_draw_rectangle((posX*CASE)+CASE-(lg/2), posY*CASE, lg,CASE, MLV_COLOR_BLUE);
		MLV_draw_rectangle(posX*CASE, (posY*CASE)+CASE-(lg/2), CASE, lg, MLV_COLOR_BLUE);
	}
	MLV_actualise_window();
}

void dessineOptions(MLV_Font * font){
	MLV_Image *image = MLV_load_image("img/poison.png");
	MLV_Image *image2 = MLV_load_image("img/potion.png");

	MLV_draw_text_with_font((CASE*LARG)+8, 150, "BOUGEZ LE PION ENCADRÉ", font, MLV_COLOR_BLACK);
	MLV_draw_text_with_font((CASE*LARG)+8, 200, "CLIQUEZ ICI POUR NE RIEN FAIRE", font, MLV_COLOR_BLACK);
	MLV_draw_text_with_font((CASE*LARG)+8, 250, "CLIQUEZ ICI POUR FINIR TOUR", font, MLV_COLOR_BLACK);
	MLV_resize_image_with_proportions(image, CASE-10, CASE-10);
	MLV_draw_image(image, (CASE*LARG)+10, 300);
	MLV_resize_image_with_proportions(image2, CASE-10, CASE-10);
	MLV_draw_image(image2, (CASE*LARG)+10, 370);
	MLV_draw_text_with_font((CASE*LARG)+40, 300, "GUERRIER : DEVIENT SERF", font, MLV_COLOR_BLACK);
	MLV_draw_text_with_font((CASE*LARG)+40, 320, "SERF : MEURT", font, MLV_COLOR_BLACK);
	MLV_draw_text_with_font((CASE*LARG)+40, 372, "GUERRIER : CRÉE UNE UNITÉ", font, MLV_COLOR_BLACK);
	MLV_draw_text_with_font((CASE*LARG)+40, 392, "SERF : DEVIENT GUERRIER", font, MLV_COLOR_BLACK);
	MLV_actualise_window();
	MLV_free_image(image);
	MLV_free_image(image2);
}

void afficheErreur(char *chaine, MLV_Font * font2){
	MLV_draw_text_with_font((CASE*LARG)+8, 100, chaine, font2, MLV_COLOR_BLACK);
	MLV_actualise_window();
	MLV_wait_seconds(1);
	MLV_draw_filled_rectangle(CASE*LARG+2,100,250, 50,MLV_COLOR_WHITE);
	MLV_actualise_window();
}

void dessineCadrillage(){
	int i, j;
	for(i = CASE; i <= CASE*LARG; i += CASE){
		MLV_draw_filled_rectangle(i, 0, INTERLIGNE, CASE*LONG, MLV_COLOR_BLACK);
	}
	for(j = CASE; j < CASE*LONG; j += CASE){
		MLV_draw_filled_rectangle(0, j,CASE*LARG,INTERLIGNE, MLV_COLOR_BLACK);
	}
	MLV_actualise_window();

}

void dessineGuerrier(char couleur, int posX, int posY){
	if (couleur == ROUGE){
		MLV_Image *image = MLV_load_image("img/guerrier-rouge.png");
		MLV_resize_image_with_proportions(image, CASE-5, CASE-5);
		MLV_draw_image(image, (posX*CASE)+4, (posY*CASE)+4);
		MLV_free_image(image);
	}
	else{
		MLV_Image *image = MLV_load_image("img/guerrier-bleu.png");
		MLV_resize_image_with_proportions(image, CASE-5, CASE-5);
		MLV_draw_image(image, (posX*CASE)+4, (posY*CASE)+4);
		MLV_free_image(image);
	}
}

void dessineSerf(char couleur, int posX, int posY){
	if (couleur == ROUGE){
		MLV_Image *image = MLV_load_image("img/serf-rouge.png");
		MLV_resize_image_with_proportions(image, CASE-5, CASE-5);
		MLV_draw_image(image, (posX*CASE)+10, (posY*CASE)+4);
		MLV_free_image(image);
	}
	else{
		MLV_Image *image = MLV_load_image("img/serf-bleu.png");
		MLV_resize_image_with_proportions(image, CASE-5, CASE-5);
		MLV_draw_image(image, (posX*CASE)+10, (posY*CASE)+4);
		MLV_free_image(image);
	}
}

void dessineMort(int posX, int posY){
	MLV_Image *image = MLV_load_image("img/poison.png");
	MLV_resize_image_with_proportions(image, CASE-5, CASE-5);
	MLV_draw_image(image, (posX*CASE)+10, (posY*CASE)+4);
	MLV_free_image(image);
}

void dessineVie(int posX, int posY){
	MLV_Image *image = MLV_load_image("img/potion.png");
	MLV_resize_image_with_proportions(image, CASE-5, CASE-5);
	MLV_draw_image(image, (posX*CASE)+10, (posY*CASE)+4);
	MLV_free_image(image);
}

void afficheGagnant(char *chaine, MLV_Font *font3){
	MLV_draw_filled_rectangle(0, 0, CASE*LARG+250,CASE*LONG, MLV_COLOR_WHITE);
	MLV_draw_text_with_font(320, 190, chaine, font3, MLV_COLOR_BLACK);
	MLV_actualise_window();
}


void accueil(Monde *monde, MLV_Font *font3, MLV_Font *font){
	char *nom1;
	char *nom2;
	MLV_draw_text_with_font(300, 160, "JEU DE COMBAT", font3, MLV_COLOR_BLACK);
	MLV_wait_input_box_with_font(370, 300, 300, 30, MLV_COLOR_BLACK, MLV_COLOR_BLACK, MLV_COLOR_WHITE,"JOUEUR 1: ",&nom1, font);
	MLV_wait_input_box_with_font(370, 300, 300, 30, MLV_COLOR_BLACK, MLV_COLOR_BLACK, MLV_COLOR_WHITE,"JOUEUR 2: ",&nom2, font);
	strcpy(monde->nom1, nom1);
	strcpy(monde->nom2, nom2);
	free(nom1);
	free(nom2);
	MLV_actualise_window();
}

void afficheListe(UListe lst){
	UListe tmp = lst;

	while(NULL != tmp){
		printf("%c, %c, (%d,%d) -> ", tmp->couleur, tmp->genre, tmp->posX, tmp->posY);
		tmp = tmp->suiv;
	}
	printf("\n");
}
